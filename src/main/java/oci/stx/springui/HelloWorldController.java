package oci.stx.springui;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    @GetMapping("/s")
    public String sayHello() {
        return "Hello, World!! #1 Edit";
    }

}
