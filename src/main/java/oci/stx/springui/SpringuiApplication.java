package oci.stx.springui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringuiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringuiApplication.class, args);
	}

}
